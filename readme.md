#Welcome to Orangear SDK

Latest version 1.7.0

This SDK helps you monetizing your application. We have implemented up-to-date tools to make your experience positive and your profits - high. That’s why this iOS SDK features:

flexible design;
fast offers import;
smooth integration.

###Attention:

SDK supports iOS versions 8.0+
At the moment we only deal with applications published at iTunes.
Smart system won’t let iPad only offers to be shown on iPhone.

In this manual we’ll guide you through SDK installation, settings, implementing offerwall and managing offers in your publisher’s account.

#Setting up SDK

Our SDK provides ample opportunities for interface customization. Make your offers look native and achieve high user satisfaction.

Three types of columns are set using ColumnOfferType:
- OneColumnOffer - for one offer column that will fill the whole screen;
- TwoColumnOffer - for two offer columns;
- ThreeColumnOffer - for three offer columns.

2. In addition, you can use the following parameters for setting up colors: 
the color of Install button is set using parameter colorButtonInstall;
the color of the offerwall background is set using colorTheme;
the font is set using fontCustomTitle and fontCustomDescription. 
the background of the specific offer is set using parameter colorOfferBackground;
widthShadow is used to set the width of the offer shading.
To change the header of the offers use:
```
[[OrangeManager sharedObject] setTitleOfferwall:@"Offerwall"]; 
```

Default values:

```
Examples:
          1. vc.columnType = OneColumnOffer;
           [[OrangeManager sharedObject] setColorButtonInstall:[sender backgroundColor]];
           [[OrangeManager sharedObject] setColorOfferBackground:[UIColor whiteColor]];
           [[OrangeManager sharedObject] setWidthShadow:2.0f];
           [[OrangeManager sharedObject] setFontCustomTitle:[UIFont boldSystemFontOfSize:10.0f]];
           [[OrangeManager sharedObject] setFontCustomDescription:[UIFont systemFontOfSize:10.0f]];
           [[OrangeManager sharedObject] setColorTheme:[sender backgroundColor]];
          2.  vc.columnType = TwoColumnOffer;
           [[OrangeManager sharedObject] setColorButtonInstall:[sender backgroundColor]];
           [[OrangeManager sharedObject] setColorOfferBackground:[UIColor whiteColor]];
           [[OrangeManager sharedObject] setWidthShadow:2.0f];
           [[OrangeManager sharedObject] setFontCustomTitle:[UIFont boldSystemFontOfSize:12.0f]];
           [[OrangeManager sharedObject] setFontCustomDescription:[UIFont systemFontOfSize:12.0f]];
           [[OrangeManager sharedObject] setColorTheme:[UIColor greenColor]];
          3.  vc.columnType = ThreeColumnOffer;
           [[OrangeManager sharedObject] setColorButtonInstall:[sender backgroundColor]];
           [[OrangeManager sharedObject] setColorOfferBackground:[UIColor colorWithRed:9.0/255 green:169.0/255 blue:236.0/255 alpha:1.0]];
           [[OrangeManager sharedObject] setWidthShadow:2.0f];
           [[OrangeManager sharedObject] setFontCustomTitle:[UIFont boldSystemFontOfSize:10.0f]];
           [[OrangeManager sharedObject] setFontCustomDescription:[UIFont systemFontOfSize:10.0f]];
           [[OrangeManager sharedObject] setColorTheme:[UIColor whiteColor]];
```




The table shows all interface options with the settings described above.

#SDK installation and setup

Download SDK and add it to the project.
Set SDK parameters.
Choose sections that should display Offerwall.
Set Offerwall parameters.
Show Offerwall in the most suitable way.

Adding SDK to the project.

Download the SDK from the Applications sub-section. Unzip the archive and drag’n’drop files to the project folder.


After that SDK settings will become available to you.

2. Fill SDK parameters in AppDelegate section of your app or its analog:
token of the application;
APP_ID of the application.

Hint!

You can find your publisher’s token and app_id in your Publisher’s account (Applications sub-section).

Example:
```
   [[OrangeManager sharedObject] setToken:@"THIS_IS_THE_TEST_TOKEN"];
   [[OrangeManager sharedObject] setAppId:@"THIS_IS_THE_TEST_APP_ID"];
```


3. Next, activate header file OrangeManager.h in the necessary section of the app.

4. Set offerwall parameters using the tool specified in customization section.

5. Show offerwall in the most suitable way. Usually, you can use present mode for that.



#Sandbox

Sandbox serves as a black box with offers. It is pushing dummy hardcoded set-off offers to your SDK for testing purposes. You can use it to test the offerwall visual layout.

As opposed to approved offers, Sandbox offers can’t be sorted. They are imported as a preset list.

Sandbox is activated in your publisher’s account at the Platform. 



#Publisher’s Acсount

After you have added your first application, 3 new sub-sections will appear in your account:

Applications. There you can:
add and delete applications;
download the appropriate SDK for each application;
change the application name for our Platform;
activate Sandbox;
get your token and app_id.

To add an application, click Add Application and enter the name and its store URL. Be attentive entering the URL, as you can not change it after the application will be added.



If you check the Sandbox option, your Offerwall will start displaying test offers. If you uncheck Sandbox, only approved offers will be shown.

No offers can be modified in the SDK section of the Platform. You can’t add your creatives here.

Offers. This section duplicates items only from the Approved Offers section. Add or remove already approved offers from SDK rotation.



Manage. Align offers in the desired sequence on this page. Initially, the Offerwall will display offers in that sequence. If you have several applications, you can set different presets for each of them.



#Sorting offers

If you have more than 1 application in your SDK section, you can set the way you would like to sort and display offers individually for each of them. To do that, go to Publisher’s account>SDK>Manage and choose the necessary app in the dropdown.



There are 2 modes of displaying offers:

Sandbox. This mode is set by default when you add new applications and appears as a black box. In this mode, your application displays test offers. They can’t be seen in your publisher’s account. Their sequence can’t be managed.

Carousel. When you uncheck the Sandbox, your application is set to Carousel mode, where it displays only offers that were added by the publisher. The display sequence is set in Publisher’s Account>SDK>Manage. Drag’n’drop offers in the order you want them to appear.

Showing offers

Offerwall sets a certain order of displaying ads. At first appearance, offers will be displayed in the sequence that you have set in the Publisher’s account. Upon the next appearance of Offerwall (without restarting the app), offers that were shown to the user, will be moved down the list. 

Note: this logic uses dynamically changing value. Try to keep 9-10 offers in the rotation, to make sure that the carousel is working properly.

On restarting an application, ads will be shown in the initial order again. To change the sequence of the approved offers, go to the publisher’s account.

#Aditor

SDK allows to open instances sent by a server and display them as set in templates. 
For such cases, create a controller and specify the title of an instance that has to be displayed by titleInstance property.
titleInstance property is set in the account as a string value, such as "Some App"
That is how interaction with Aditor will look like:
 
Example with step-by-step integration.
 
Begin by plugging in the title file:
```
#import "InstanceController.h"
```
2.Create a sample of this controller and then display it:
 
Note! All steps should be done in the selected class.
```
     InstanceController *instance = [[InstanceController alloc] initWithNibName:@"InstanceController" bundle:nil];
     instance.titleInstance = @"Some app2";
     __weak __block YourDialog *weakSelf = self;
     [[OrangeManager sharedObject] getOffersWithCompletionAditor:^(APIResult result, NSString *description, NSMutableArray<OfferModel *> *offers) {
         if ([OrangeManager sharedObject].offers.count > 0)
             [weakSelf.navigationController pushViewController:instance animated:YES];
     }];
     [self.navigationController pushViewController:instance animated:YES];
```

In this case "Some app" is the name of the instance set in your account. 

Here is an order of code implementation
While starting SDK, a list of instances set by the developer for an application is attached automatically.
As you specify titleInstance and run a controller (either in modal version or push navigation), a request is sent. Then you get a random offer similarly to the offerwall workflow.  
After getting appId from the offers’ list, if there is any, scraper from AppStore is called to get information about an application. 
Information is compared to the template sent by the server for the instance. 
Close button will appear after timeToSkip will reach a value specified for the instance template.
After a click on Install button, the track method is called. The method is used to install an application via link set for the specific offer.

#Additional info

At the moment only Organic Offerwall campaign mode is available in SDK. In upcoming releases, we will add Rewarded Offerwall option, Interstitials, Banners, and Video. We will keep you posted on our updates.

If you have any questions, please contact our support team.