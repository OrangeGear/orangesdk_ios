
#import <Foundation/Foundation.h>

@interface NSDictionary (Maker)

- (NSDictionary *)dictionaryByAddingDictionary:(NSDictionary *)data;

@end
