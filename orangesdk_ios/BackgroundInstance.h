//
//  BackgroundInstance.h
//  AppAve
//
//  Created by Viktorianec on 15.03.2018.
//  Copyright © 2018 Viktorianec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BackgroundInstance : NSObject
@property (nonatomic, strong) NSString *colorHex;
@property (nonatomic, assign) float transparency;

+ (instancetype)backgroundInstanceWithDict:(NSDictionary *)dictionary;

@end
