//
//  ItemStoreModel.h
//  AppAve
//
//  Created by Viktorianec on 06.02.2018.
//  Copyright © 2018 Viktorianec. All rights reserved.
//

//https://itunes.apple.com/lookup?id=
#import <Foundation/Foundation.h>

@interface ItemStoreModel : NSObject
@property (nonatomic, assign) BOOL gameCenterEnabled;
@property (nonatomic, strong) NSString *developerURL;
@property (nonatomic, strong) NSString *art60URL;
@property (nonatomic, strong) NSString *art100URL;
@property (nonatomic, strong) NSString *art512URL;
@property (nonatomic, strong) NSArray *screenshotsUrlIpad;
@property (nonatomic, strong) NSArray *supportedDevices;
@property (nonatomic, strong) NSArray *screenshotsUrlIphone;
@property (nonatomic, strong) NSString *adWarnings;
@property (nonatomic, strong) NSString *platforms;
@property (nonatomic, strong) NSString *trackCensoredName;
@property (nonatomic, strong) NSString *trackViewUrl;
@property (nonatomic, strong) NSString *contentAdvisoryRating;
@property (nonatomic, strong) NSArray *languageCodes;
@property (nonatomic, strong) NSString *sellerUrl;
@property (nonatomic, strong) NSString *currency;
@property (nonatomic, strong) NSString *versionApp;
@property (nonatomic, assign) NSInteger artistId;
@property (nonatomic, assign) NSInteger appId;
@property (nonatomic, assign) float sizeApp;
@property (nonatomic, strong) NSString *artistName;
@property (nonatomic, strong) NSString *releaseNotes;
@property (nonatomic, strong) NSArray *genresArray;
@property (nonatomic, assign) float price;
@property (nonatomic, strong) NSString *descriptionApp;
@property (nonatomic, strong) NSString *bundleId;
@property (nonatomic, strong) NSString *primaryGenre;
@property (nonatomic, strong) NSString *dateRelease;
@property (nonatomic, strong) NSString *lastVersionDate;
@property (nonatomic, assign) float minimumOSVersion;
@property (nonatomic, strong) NSString *formattedPrice;

+ (instancetype)itemStoreWithDictionary:(NSDictionary *)dictionary;

@end
