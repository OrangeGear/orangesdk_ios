
#import <UIKit/UIKit.h>

@interface NSDictionary (Value)

- (id)getObjectForKey:(id)key;

- (NSString *)stringValueForKey:(id)key;
- (NSInteger)integerValueForKey:(id)key;
- (CGFloat)floatValueForKey:(id)key;
- (double)doubleValueForKey:(id)key;
- (BOOL)boolValueForKey:(id)key;
- (NSData *)dataValueForKey:(id)key;
- (NSArray *)arrayValueForKey:(id)key;
- (NSDictionary *)dictionaryValueForKey:(id)key;

@end
