//
//  Screen.h
//  AppAve
//
//  Created by Viktorianec on 15.03.2018.
//  Copyright © 2018 Viktorianec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FrameBase.h"

@interface Screen : NSObject
@property (nonatomic, strong) FrameBase *baseFrame;

+ (instancetype)screenWithDict:(NSDictionary *)dictionary;
@end
