//
//  Binding.h
//  AppAve
//
//  Created by Viktorianec on 13.07.17.
//  Copyright © 2017 Viktorianec. All rights reserved.
//

#ifndef Binding_h
#define Binding_h
extern "C" {
    void _startSDK (const char* appId, const char* appToken);
    
    void _showOffers (int typeOfferwall);
    
    int _countReward ();
    
    char* _deviceId();
}
#endif /* Binding_h */
