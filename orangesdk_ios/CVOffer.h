
#import <UIKit/UIKit.h>
#import "OfferModel.h"
@interface CVOffer : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconInterstitial;
@property (weak, nonatomic) IBOutlet UIButton *buttonInstall;
@property (nonatomic, strong) NSURLSessionDataTask *imageDownloadTask;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;

- (void)setOffer:(OfferModel *)dataDict;

@end
