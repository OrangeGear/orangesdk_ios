//
//  Description.h
//  AppAve
//
//  Created by Viktorianec on 16.03.2018.
//  Copyright © 2018 Viktorianec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FrameBase.h"

@interface Description : NSObject
@property (nonatomic, strong) FrameBase *baseFrame;
@property (nonatomic, assign) NSInteger linesCount;
@property (nonatomic, strong) NSString *hexColor;

+ (instancetype)descriptionWithDict:(NSDictionary *)dictionary;
@end
