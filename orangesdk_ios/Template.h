//
//  Template.h
//  AppAve
//
//  Created by Viktorianec on 15.03.2018.
//  Copyright © 2018 Viktorianec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BackgroundInstance.h"
#import "Screen.h"
#import "Rating.h"
#import "DownloadCount.h"
#import "Description.h"
#import "Cta.h"
#import "CtaText.h"
#import "StorageLogo.h"
#import "Timer.h"
#import "CloseButton.h"
#import "ProgressLine.h"

@interface Template : NSObject

@property (nonatomic, strong) BackgroundInstance *background;
@property (nonatomic, strong) Screen *screen;
@property (nonatomic, strong) Rating *rating;
@property (nonatomic, strong) DownloadCount *downloadCount;
@property (nonatomic, strong) Description *descriptionInstance;
@property (nonatomic, strong) Cta *cta;
@property (nonatomic, strong) CtaText *ctaText;
@property (nonatomic, strong) StorageLogo *storageLogo;
@property (nonatomic, strong) Timer *timer;
@property (nonatomic, strong) CloseButton *closeButton;
@property (nonatomic, strong) ProgressLine *progressLine;

+ (instancetype)templateWithDictionary:(NSDictionary *)dictionary;

@end
