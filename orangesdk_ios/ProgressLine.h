//
//  ProgressLine.h
//  AppAve
//
//  Created by Viktorianec on 16.03.2018.
//  Copyright © 2018 Viktorianec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProgressLine : NSObject
@property (nonatomic, assign) float height;
@property (nonatomic, strong) NSString *hexColor;

+ (instancetype)progressLineWithDict:(NSDictionary *)dictionary;

@end
