//
//  Instance.h
//  AppAve
//
//  Created by Viktorianec on 13.03.2018.
//  Copyright © 2018 Viktorianec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Template.h"

@interface Instance : NSObject
@property (nonatomic, assign) NSInteger idInstance;
@property (nonatomic, assign) NSUInteger appId;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) BOOL hasCarousel;
@property (nonatomic, assign) BOOL hasVideo;
@property (nonatomic, assign) BOOL isErpc;
@property (nonatomic, assign) NSInteger timeToSkip;

@property (nonatomic, strong) Template *templateInterface;

+ (instancetype)instanceWithDictionary:(NSDictionary *)dictionary;

@end
