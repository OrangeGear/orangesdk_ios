
#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"
#import "OfferModel.h"
#import "Instance.h"

@class OrangeGearRewarded;

@protocol OrangeGearRewardedDelegate <NSObject>

@optional

- (void)onGetReward:(NSInteger)sizeReward;

@end

typedef enum APIResult
{
    RESULT_OK = 1,
    RESULT_ERROR = 0
} APIResult;

typedef enum TypeOrange
{
    TypeOrangeOfferwall,
    TypeOrangeRewarded
} TypeOrange;

typedef enum TypeRewarded
{
    QuickRewardsType,
    PostbackRewardsType
} TypeRewarded;


@interface OrangeManager : NSObject
+ (instancetype)sharedObject;

//settings for API
@property (nonatomic, retain) NSString *token;
@property (nonatomic, retain) NSString *deviceId;
@property (nonatomic, retain) NSString *appId;

//settings for Interface
@property (nonatomic, assign) TypeOrange orangeType;
@property (nonatomic, assign) BOOL isQuickRewarded;
@property (nonatomic, strong) UIColor *colorButtonInstall;
@property (nonatomic, strong) UIColor *colorTheme;
@property (nonatomic, retain) NSString *titleOfferwall;
@property (nonatomic, strong) UIFont *fontCustomTitle;
@property (nonatomic, strong) UIFont *fontCustomDescription;
@property (nonatomic, strong) UIColor *colorOfferBackground;
@property (nonatomic, assign) float widthShadow;
@property (nonatomic, assign) NSInteger exchangeRate;

@property (nonatomic, assign) NSInteger countRewardTotal;

@property (nonatomic, retain) NSString *currencyName;

@property (nonatomic, strong) NSArray <Instance*> *instances;

@property (nonatomic, strong) NSMutableArray <OfferModel*> *offers;

@property (weak, nonatomic) id <OrangeGearRewardedDelegate> rewardedDelegate;

- (void)start;
- (void)getOffersWithCompletion:(void(^)(APIResult result, NSString *description, id object))finishBlock;

- (void)getClickForOffer:(NSInteger)offerID withResult:(void(^)(APIResult result, NSString *description, id object))finishBlock;

- (void)getOffersWithCompletionAditor:(void(^)(APIResult result, NSString *description, NSMutableArray <OfferModel *> *offers))finishBlock;

- (void)redirectForItems:(NSString *)jsonFormedString andItemId:(NSInteger)idItem withResult:(void(^)(APIResult result, NSString *description, id object))finishBlock;

- (void)trackInstallForItems:(NSString *)jsonFormedString withResult:(void(^)(APIResult result, NSString *description, id object))finishBlock;

- (void)getDescriptionOfferWithId:(NSString *)offerId withCompletionHandler:(void (^)(NSData *))completionHandler;

- (void)addNewOfferToCache:(OfferModel *)offer;

- (BOOL)containsOfferInCache:(OfferModel *)offer;
@end
