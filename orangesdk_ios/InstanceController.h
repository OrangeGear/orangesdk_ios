//
//  InstanceController.h
//  AppAve
//
//  Created by Viktorianec on 16.03.2018.
//  Copyright © 2018 Viktorianec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Instance.h"

@interface InstanceController : UIViewController
@property (nonatomic, strong) NSString *titleInstance;
@end
