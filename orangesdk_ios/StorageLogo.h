//
//  StorageLogo.h
//  AppAve
//
//  Created by Viktorianec on 16.03.2018.
//  Copyright © 2018 Viktorianec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FrameBase.h"

@interface StorageLogo : NSObject

@property (nonatomic, strong) FrameBase *baseFrame;
@property (nonatomic, assign) BOOL isActive;

+ (instancetype)storageLogoWithDict:(NSDictionary *)dictionary;

@end
