
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ColumnOfferType) {
    OneColumnOffer,
    TwoColumnOffer,
    ThreeColumnOffer,
};

@interface VCOffers : UIViewController
@property (nonatomic, retain) UIColor *themeColor;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (nonatomic, assign) ColumnOfferType columnType;

@end
