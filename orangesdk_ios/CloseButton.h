//
//  CloseButton.h
//  AppAve
//
//  Created by Viktorianec on 16.03.2018.
//  Copyright © 2018 Viktorianec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FrameBase.h"

@interface CloseButton : NSObject
@property (nonatomic, strong) FrameBase *baseFrame;
@property (nonatomic, assign) float transparency;
@property (nonatomic, strong) NSString *hexColor;

+ (instancetype)closeButtonWithDict:(NSDictionary *)dictionary;

@end
