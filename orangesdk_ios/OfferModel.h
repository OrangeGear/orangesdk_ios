//
//  OfferModel.h
//  OranGear
//
//  Created by OranGear on 16.03.17.
//  Copyright © 2017 OranGear. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OfferModel : NSObject

@property (nonatomic, strong) NSString *nameOffer;
@property (nonatomic, assign) NSInteger offerID;
@property (nonatomic, assign) NSInteger rewardAmount;
@property (nonatomic, assign) float ratingOffer;
@property (nonatomic, strong) NSString *currencyName;
@property (nonatomic, assign) float currencyCount;
@property (nonatomic, strong) NSString *descriptionOffer;
@property (nonatomic, strong) NSString *pathToImage;
@property (nonatomic, strong) NSString *previewUrl;
@property (nonatomic, strong) NSString *trackingUrl;
@property (nonatomic, strong) NSString *appId;

+(instancetype)offerWithDictionary:(NSDictionary *)dictionary;

@end
