//
//  Cta.h
//  AppAve
//
//  Created by Viktorianec on 16.03.2018.
//  Copyright © 2018 Viktorianec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FrameBase.h"

@interface Cta : NSObject

@property (nonatomic, strong) FrameBase *baseFrame;
@property (nonatomic, assign) float transparency;
@property (nonatomic, assign) float radius;
@property (nonatomic, assign) BOOL isOutlineOnly;
@property (nonatomic, strong) NSString *hexColor;

+ (instancetype)ctaWithDict:(NSDictionary *)dictionary;

@end
