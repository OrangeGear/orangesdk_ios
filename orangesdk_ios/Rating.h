//
//  Rating.h
//  AppAve
//
//  Created by Viktorianec on 16.03.2018.
//  Copyright © 2018 Viktorianec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FrameBase.h"

@interface Rating : NSObject
@property (nonatomic, strong) FrameBase *baseFrame;
@property (nonatomic, assign) BOOL isActive;
@property (nonatomic, strong) NSString *hexColor;

+ (instancetype)ratingWithDict:(NSDictionary *)dictionary;

@end
