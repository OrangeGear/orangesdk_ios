//
//  Timer.h
//  AppAve
//
//  Created by Viktorianec on 16.03.2018.
//  Copyright © 2018 Viktorianec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FrameBase.h"

@interface Timer : NSObject

@property (nonatomic, strong) FrameBase *baseFrame;
@property (nonatomic, assign) BOOL isActive;

+ (instancetype)timerWithDict:(NSDictionary *)dictionary;

@end
