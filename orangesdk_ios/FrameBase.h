//
//  FrameBase.h
//  AppAve
//
//  Created by Viktorianec on 16.03.2018.
//  Copyright © 2018 Viktorianec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FrameBase : NSObject
@property (nonatomic, assign) float width;
@property (nonatomic, assign) float height;
@property (nonatomic, assign) float x;
@property (nonatomic, assign) float y;

+ (instancetype)frameWithDict:(NSDictionary *)dictionary;

@end
