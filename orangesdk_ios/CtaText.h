//
//  CtaText.h
//  AppAve
//
//  Created by Viktorianec on 16.03.2018.
//  Copyright © 2018 Viktorianec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CtaText : NSObject
@property (nonatomic, strong) NSString *ctaText;
@property (nonatomic, strong) NSString *hexColor;

+ (instancetype)ctaTextWithDict:(NSDictionary *)dictionary;

@end
